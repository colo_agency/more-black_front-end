export const state = () => ({
  headerLinks: [
    { to: '/', text: 'Главная' },
    { to: '/catalog', text: 'Магазин' },
    { to: '/about', text: 'О нас' },
    { to: '/contacts', text: 'Контакты' },
  ],
  footerLinks: [
    { to: '/', text: 'Главная' },
    { to: '/catalog', text: 'Магазин' },
    { to: '/about', text: 'О нас' },
    { to: '/contacts', text: 'Контакты' },
    { to: '/privacy', text: 'Privacy policy' },
  ],
});
